## elk

> elk镜像基于sebp/elk镜像构建，主要提供日志查询和分析

### 如何构建此镜像

```
git clone https://git.oschina.net/csphere/elk.git
cd elk
docker build -t csphere/elk .
```

### 运行容器

```
docker run -d -p 5000:5000 -p 9200:9200 -p 5601:5601 -e ES_MIX_MEM=64m -e ES_MAX_MEM=512m --name elk --restart=always csphere/elk
```

### 配置logstash

```
docker exec -it elk /bin/bash
/opt/logstash/bin/logstash -e 'input { stdin { } } output { elasticsearch { host => localhost } }'
this is a dummy entry
```

测试:`http://<your-host>:9200/_search?pretty`